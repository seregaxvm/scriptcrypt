#!/bin/bash

case "$1" in
    (-h | --help)
        echo "db2json <output path>"
        exit 0 ;;
esac

if [[ -d "$1" ]]; then
    FILEOUT="$1"
elif [[ -d "${1%/*}" ]]; then
	FILEOUT="$1"
else
    FILEOUT="$HOME"/scriptcrypt.json
fi

errorexit(){
    echo "$1"
    exit 1
}

python3 --version > /dev/null || errorexit "No python3 found" 

FULL_PATH=$(realpath "$0")
cd "${FULL_PATH%/*}"/../..

python3 -c 'from scriptcrypt.db.db2json import run as r; r("'"$FILEOUT"'")' || errorexit "Conversion failed" 

echo "Output has been writen to the $FILEOUT"
