#!/bin/bash

errorexit(){
    echo "$1"
    exit 1
}

case "$1" in
    (-h | --help)
        echo "json2db <json input> <output path>"
        exit 0 ;;
esac

if [[ -f $1 ]]; then
    FILEIN=$1
    FILEOUT=$2
else
    FILEIN="$HOME"/scriptcrypt.json
    FILEOUT="$HOME"/scriptcrypt.db
fi

if [[ ! -f "$FILEIN" ]] | [[ ! -d "${FILEOUT%/*}" ]]; then
    errorexit "Error in filepaths"    
fi

if [[ -f "$FILEOUT" ]]; then
    errorexit "Not overwriting existing file"    
fi

python3 --version > /dev/null || errorexit "No python3 found" 

FULL_PATH=$(realpath "$0")
cd "${FULL_PATH%/*}"/../..

python3 -c 'from scriptcrypt.db.json2db import run as r; r("'"$FILEIN"'","'"$FILEOUT"'")' || errorexit "Conversion failed"

echo "$FILEIN was converted to $FILEOUT"
